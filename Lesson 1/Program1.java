package HomeWork1;

class Program1 {
    public static void main(String[] args) {
        int number = 12345;
        int digitsSum = 0;
        int currentDigit;
        currentDigit = number % 10; //5
        digitsSum = digitsSum + currentDigit; //digitsSum 0 + 5
        number = number / 10; //5 (1234)
        currentDigit = number % 10; //4
        digitsSum = digitsSum + currentDigit; //digitsSum 5 + 4
        number = number / 10; //4 (123)
        currentDigit = number % 10; //3
        digitsSum = digitsSum + currentDigit; //digitsSum 9 + 3
        number = number / 10; //3 (12)
        currentDigit = number % 10; //2
        digitsSum = digitsSum + currentDigit; //digitsSum 12 + 2
        number = number / 10; //2 (1)
        digitsSum = digitsSum + number; //digitsSum 14 + 1
        System.out.println(digitsSum);
    }
}
