package HomeWork1;

import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        System.out.print("Enter the number: ");
        boolean isPrime = true;
        String prime = " - number is prime";
        String composite = " - number is composite";
        Scanner scanner = new Scanner(System.in);
        int b;
        int a;
        int mul = 1;
        do {
            b = scanner.nextInt();
            a = b;
            if (b != 0) {
                int sum = 0;
                for (int i = 0; i < 10; i++) {
                    sum += a % 10;
                    a /= 10;
                }
                for (int j = 2; j < sum; j++) {
                    if (sum % j == 0) {
                        isPrime = false;
                        break;
                    } else {
                        isPrime = true;
                    }
                }
                if (isPrime) {
                    mul *= b;
                    System.out.println("Sum of numbers: " + sum + prime);
                } else {
                    System.out.println("Sum of numbers: " + sum + composite);
                }
                System.out.print("Enter the number: ");
            } else {
                System.out.println("Number is 0");
            }
        }
        while (b != 0);
        if (mul <= 1) {
            System.out.println("No prime numbers");
        } else {
            System.out.println("Answer: multiplication of prime numbers - " +mul);
        }
    }
}
