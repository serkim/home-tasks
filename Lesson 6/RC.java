package HomeWork6;

import java.util.Scanner;

public class RC {
    private static final int MAX_CHANNEL = 5;
    private TV tv;
    private Channel[] channels;
    private int channelsCount;

    public RC() {
        this.channels = new Channel[MAX_CHANNEL];
    }
    public void addChannel(Channel channel) {
        if (channelsCount < MAX_CHANNEL) {
            channels[channelsCount] = channel;
            channelsCount++;
        }
    }
    public void choiceChannel() {
        System.out.print("Выберите номер канала от 0 до 4: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(channels.length);
        System.out.println("Вы выбрали канал номер " + channels[n].getNumber() + " - " + channels[n].getName());
        channels[n].showProgram();
    }
}