package HomeWork6;

import java.util.Scanner;

public class TV {
    private static final int MAX_CHANNEL = 5;
    private final Channel[] channels;
    private int channelsCount;
    private RC rc;

    public TV() {
        this.channels = new Channel[MAX_CHANNEL];
    }
    public void addChannel(Channel channel) {
        if (channelsCount < MAX_CHANNEL) {
            channels[channelsCount] = channel;
            channelsCount++;
        }
    }
    public void requestChannel() { // Метод для выбора каналов с телевизора
        System.out.print("Введите номер канала: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(channels.length);
        System.out.println("Вы выбрали канал номер " + channels[n].getNumber() + " - " + channels[n].getName());
        channels[n].showProgram();
    }
}
