package HomeWork6;

public class Main {
    public static void main(String[] args) {
//Инициализация каналов
        Channel first = new Channel(0, "Первый");
        Channel rossia1 = new Channel(1, "Россия 1");
        Channel match = new Channel(2, "МАТЧ!ТВ");
        Channel culture = new Channel(3, "Культура");
        Channel rossia24 = new Channel(4, "Россия 24");
//Инициализация программ
        Program p1 = new Program("Доброе утро");
        Program p2 = new Program("Новости");
        Program p3 = new Program("Вечерний Ургант");
        Program p4 = new Program("Утро России");
        Program p5 = new Program("Вести");
        Program p6 = new Program("Привет, Андрей!");
        Program p7 = new Program("Евро 2020");
        Program p8 = new Program("Большая Игра");
        Program p9 = new Program("Все на МАТЧ!");
        Program p10 = new Program("Новости Культуры");
        Program p11 = new Program("Правила жизни");
        Program p12 = new Program("Спокойной ночи, малыши");
        Program p13 = new Program("Мобильный репортер");
        Program p14 = new Program("Москва.Кремль.Путин");
        Program p15 = new Program("Вести.net");
//Инициализация телевизора и пульта управления
        TV tv = new TV();
        RC rc = new RC();
//Добавление программ в каналы
        first.addProgram(p1);
        first.addProgram(p2);
        first.addProgram(p3);
        rossia1.addProgram(p4);
        rossia1.addProgram(p5);
        rossia1.addProgram(p6);
        match.addProgram(p7);
        match.addProgram(p8);
        match.addProgram(p9);
        culture.addProgram(p10);
        culture.addProgram(p11);
        culture.addProgram(p12);
        rossia24.addProgram(p13);
        rossia24.addProgram(p14);
        rossia24.addProgram(p15);
//Добавление каналов в телевизор
        tv.addChannel(first);
        tv.addChannel(rossia1);
        tv.addChannel(match);
        tv.addChannel(culture);
        tv.addChannel(rossia24);
//Добавление каналов в пульт управления
        rc.addChannel(first);
        rc.addChannel(rossia1);
        rc.addChannel(match);
        rc.addChannel(culture);
        rc.addChannel(rossia24);
//Вызов пульта управления
        rc.choiceChannel();
    }
}