package HomeWork6;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class Channel {
    private static final int MAX_PROGRAM = 3;
    private int number;
    private String name;
    private Program[] programs;
    private int programsCount;

    public Channel(int number, String name) {
        this.number = number;
        this.name = name;
        this.programs = new Program[MAX_PROGRAM];
    }
    public void addProgram(Program program) {
        if (programsCount < MAX_PROGRAM) {
            programs[programsCount] = program;
            programsCount++;
        }
    }
    public void showProgram() {
        Random random = new Random();
        int rnd = random.nextInt(programs.length);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime time = LocalTime.now();
        String time2 = formatter.format(time);
        System.out.println(time2 + " - " + "Сейчас в эфире " + " - " + programs[rnd].getName());
    }

    public int getNumber() {
        return number;
    }
    public String getName() {
        return name;
    }
}





