package Example;

import java.util.Arrays;
import java.util.Scanner;

public class MinmaxChange {
    public int[] minmax() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("You selected: to get change min and max of array elements ");
        System.out.println("Enter quantity of elements: ");
        int n = scanner.nextInt();
        int[] array = new int[n];
        System.out.println("Enter elements: ");

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        int min = 0;
        int max = 0;
        int temp;

        for (int i = 0; i < array.length; i++) {
            if ( array[min] > array[i] )
                min = i;
            if ( array[max] < array[i] )
                max = i;
        }
        System.out.println("Entered array: " + Arrays.toString(array));
        System.out.println("Min element is - " + array[min]);
        System.out.println("Max element is - " + array[max]);
        temp = array[min];
        array[min] = array[max];
        array[max] = temp;
        System.out.print("Changed array: ");
        return array;
    }
}