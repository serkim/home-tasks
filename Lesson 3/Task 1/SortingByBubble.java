package Example;

import java.util.Arrays;
import java.util.Scanner;

public class SortingByBubble {
    public int[] bubble() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("You selected: to get sorting array elements by bubble ");
        System.out.println("Enter quantity of elements: ");
        int n = scanner.nextInt();
        int[] array = new int[n];
        System.out.println("Enter elements: ");
        int a;
        int b;
        int numberOFpassage = 0;


        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println("Entered array: " + Arrays.toString(array));
        System.out.println();

        boolean isSorted = false;
        int temp;

        while(!isSorted) {
            isSorted = true;

            for (int i = 0; i < array.length-1; i++) {
                if(array[i] > array[i+1]){
                    isSorted = false;

                    temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    a = array[i];
                    b = array[i+1];
                    numberOFpassage += 1;

                    System.out.println("Number of passage: " + numberOFpassage);
                    System.out.println("Changed pair: " + a +" - "+ b);
                    System.out.println("Sorted array: " + Arrays.toString(array));
                    System.out.println();
                }
            }
        }
        System.out.print("Final sorted array: ");
        return array;
    }
}