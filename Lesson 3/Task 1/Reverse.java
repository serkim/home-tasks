package Example;

import java.util.Scanner;

public class Reverse {
    public int[] revArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("You selected: to get rearrange array elements in reverse order ");
        System.out.println("Enter quantity of elements: ");
        int n = scanner.nextInt();
        int[] array = new int[n];
        System.out.println("Enter elements: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        for (int i = 0; i < array.length/2; i++) {
            int a = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = a;
        }
        System.out.println("Array elements in reverse order ");
        return array;
    }
}



