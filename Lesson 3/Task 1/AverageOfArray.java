package Example;

import java.util.Arrays;
import java.util.Scanner;

public class AverageOfArray {
    public double average() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("You selected: to get average of array elements ");
        System.out.println("Enter quantity of elements: ");
        int a = scanner.nextInt();
        int[] array = new int[a];
        System.out.println("Enter elements: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println("Entered array: " + Arrays.toString(array));
        double average = 0;
        for (int i = 0; i < array.length; i++) {
            average += array[i];
        }
        average /= array.length;
        System.out.print("Average of array elements: ");
        return average;
    }
}
