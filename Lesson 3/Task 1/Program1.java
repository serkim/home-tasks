package Example;

import java.util.Arrays;
import java.util.Scanner;

public class Program1 {
    public static void main(String[] args) {
        System.out.println("Hello! What operation would you like to perform?");
        System.out.println("- Enter [1] to get the sum of array elements");
        System.out.println("- Enter [2] to get rearrange array elements in reverse order");
        System.out.println("- Enter [3] to get average of array elements");
        System.out.println("- Enter [4] to get change min and max of array elements");
        System.out.println("- Enter [5] to get sorting array elements by bubble");
        System.out.println("- Enter [6] to get converting array in a number");
        System.out.println();
        System.out.print("Enter the number please: ");
        Scanner scanner = new Scanner(System.in);
        int n = 0;
        while (n == 0 || n > 6) {
            n = scanner.nextInt();
            System.out.println("You entered the wrong number!!!");
            System.out.println("Please try again");
            System.out.print("Enter the number please: ");
            
        if (n == 1){
            SumCopy sumCopy = new SumCopy();
            int choice1 = sumCopy.summa();
            System.out.println(choice1);
        }

        if (n == 2){
            Reverse reverse = new Reverse();
            int[] choice2 = reverse.revArray();
            System.out.println(Arrays.toString(choice2));
        }

        if (n == 3){
            AverageOfArray averageOfArray = new AverageOfArray();
            double choice3 = averageOfArray.average();
            System.out.println(choice3);
        }

        if (n == 4){
            MinmaxChange minmaxChange = new MinmaxChange();
            int[] choice4 = minmaxChange.minmax();
            System.out.println(Arrays.toString(choice4));
        }

        if (n == 5){
            SortingByBubble sortingByBubble = new SortingByBubble();
            int[] choice5 = sortingByBubble.bubble();
            System.out.println(Arrays.toString(choice5));
        }

        if (n == 6){
            NumberConverting numberConverting = new NumberConverting();
            int choice6 = numberConverting.numbConvert();
            System.out.println(choice6);
        }               
    }
}
}

