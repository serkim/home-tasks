package Example;

import java.util.Arrays;
import java.util.Scanner;

public class NumberConverting {
    public int numbConvert() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("You selected: to get converting array in a number ");
        System.out.println("Enter quantity of elements: ");
        int n = scanner.nextInt();
        int[] array = new int[n];
        int number = 0;
        System.out.println("Enter elements: ");

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        for (int i = array.length - 1, b = 0; i >= 0; --i, b++) {
            int a = (int) Math.pow(10, i);
            number += array[b] * a;
        }
        System.out.println("Entered array: " + Arrays.toString(array));
        System.out.print("Converted an array to a number: ");
        return number;
    }
}
