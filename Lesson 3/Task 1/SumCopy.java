package Example;

import java.util.Arrays;
import java.util.Scanner;

    public class SumCopy {

        public int summa() {
            System.out.println("You selected: to get the sum of array elements ");
            System.out.println("Enter quantity of elements: ");
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            int[] array = new int[n];
            String result = "Sum of array elements is ";
            int sum = 0;
            System.out.println("Enter elements: ");

            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();
            }
            System.out.println("Entered array: " + Arrays.toString(array));
            for (int i = 0; i < array.length; i++) {
                sum += array[i];
            }
            System.out.print("Sum of array elements: ");
            return sum;

        }
    }

