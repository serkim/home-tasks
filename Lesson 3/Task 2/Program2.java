package HomeWork3;

import java.util.Scanner;

public class Program2 {

    public static double f(double x) {
        return 1/Math.log(x);
    }

    public static void main(String[] args) {
        System.out.println("Enter the integral limit from a to b: ");

        Scanner in = new Scanner(System.in);

        double a = in.nextDouble();
        double b = in.nextDouble();
        double integral = 0;
        double sum = 0;
        double h = 2;
        double N = (b - a) / h;

        for (int i = 1; i <= N; i += 2) {
            sum += f(a + h * (i - 1)) + 4 * f(a + h * i) + f(a + h * (i + 1));
            if (N %2 == 0){
                integral = (h / 3) * sum;
            } else {
                integral = (h / 3) * (sum -  3 * f(b) - f(b + h));
            }
        }
        System.out.println("Entered limit of integral from [" + a +"] to ["+ b +"]");
        System.out.println("Result is " + integral);
    }
}