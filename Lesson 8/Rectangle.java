package HomeWork8;

public class Rectangle extends Square implements Scale, Movement{
    private double width;

    public Rectangle(String name, double x, double y, double scale, double height, double width) {
        super(name, x, y, scale, height);
        this.width = width;
    }
    public void area(){
        double a = getHeight() * width;
        System.out.println("Area of " + name + " = " + a);
    }
    public void perimeter(){
        double p = 2 * (getHeight() * width);
        System.out.println("Perimeter of " + name + " = " + p);
    }
    public void scale(){
        double d = Math.sqrt(getHeight() * width) * 2;
        double scale = getScale() * d;
        System.out.println("Diagonal of " + name + " changed from "+ d + " to " + scale);
    }
}
