package HomeWork8;

public class Ellipse extends Circle implements Scale {

    private double smallRadius;

    public Ellipse(String name, double x, double y, double scale, double radius, double smallRadius) {
        super(name, x, y, scale, radius);
        this.smallRadius = smallRadius;
    }
    public void area() {
        double a = getRadius() * smallRadius * Math.PI;
        System.out.println("Area of " + name + " = " + a);
    }
    public void perimeter() {
        double p = 4 * (Math.PI * getRadius() * smallRadius + (getRadius() - smallRadius) * (getRadius() - smallRadius)) / (getRadius() + smallRadius);
        System.out.println("Perimeter of " + name + " = " + p);
    }
    public void scale() {
        double r = getRadius() + smallRadius;
        double scale = getScale() * r;
        System.out.println("Scale of " + name + " changed from " + r + " to " + scale);
    }
}