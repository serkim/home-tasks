package HomeWork8;

public class Main {
    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse("ellipse", 0,0, 2, 5,2);
        ellipse.setName();
        ellipse.area();
        ellipse.perimeter();
        ellipse.scale();
        ellipse.move(7,7);

        Circle circle = new Circle("circle",0, 0,0.5,3);
        circle.setName();
        circle.area();
        circle.perimeter();
        circle.scale();
        circle.move(5,5);

        Rectangle rectangle = new Rectangle("rectangle", 0, 0, 2,5,9);
        rectangle.setName();
        rectangle.area();
        rectangle.perimeter();
        rectangle.scale();
        rectangle.move(8,4);

        Square square = new Square("square", 0, 0, 2, 5);
        square.setName();
        square.area();
        square.perimeter();
        square.scale();
        square.move(6,6);
    }
}