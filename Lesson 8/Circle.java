package HomeWork8;

public class Circle extends GeometricFigure implements Scale, Movement {

    private double radius;

    public Circle(String name, double x, double y, double scale, double radius) {
        super(name, x, y, scale);
        this.radius = radius;
    }
    public void area(){
        double a = Math.PI * (radius * radius);
        System.out.println("Area of " + name + " = " + a);
    }
    public void perimeter(){
        double p = 2 * Math.PI * radius;
        System.out.println("Perimeter of " + name + " = " + p);
    }
    @Override
    public void scale() {
        double scale = radius * getScale();
        System.out.println("Radius of " + name + " changed from "+ radius + " to " + scale);
    }
    public double getRadius() {
        return radius;
    }

}