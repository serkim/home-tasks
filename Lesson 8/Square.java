package HomeWork8;

public class Square extends GeometricFigure implements Scale, Movement{
    private double height;

    public Square(String name, double x, double y, double scale, double height) {
        super(name, x, y, scale);
        this.height = height;
    }
    public void area(){
        double a = height * height;
        System.out.println("Area of " + name + " = " + a);
    }
    public void perimeter(){
        double p = 4 * height;
        System.out.println("Perimeter of " + name + " = " + p);
    }
    public void scale(){
        double d = Math.sqrt(2) * height;
        double scale = getScale() * d;
        System.out.println("Diagonal of " + name + " changed from "+ d + " to " + scale);
    }
    public double getHeight() {
        return height;
    }
}