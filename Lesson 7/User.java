package HomeWork7;

public class User{
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;
    private double height;
    private double weight;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public double getHeight() {
        return height;
    }

    public double getWeight() {
        return weight;
    }

    public static class builder{

        private final User newUser;

        public builder(){
            newUser = new User();
        }
        public builder firstName(String firstName){
            newUser.firstName = firstName;
            return this;
        }
        public builder lastName(String lastName) {
            newUser.lastName = lastName;
            return this;
        }

        public builder age(int age){
            newUser.age = age;
            return this;
        }
        public builder isWorker(boolean isWorker){
            newUser.isWorker = isWorker;
            return this;
        }
        public builder height(double height){
            newUser.height = height;
            return this;
        }
        public builder weight(double weight) {
            newUser.weight = weight;
            return this;
        }
        public User build(){
            return newUser;
        }
    }
}