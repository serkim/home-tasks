package HomeWork4;

import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Fibonacci number: ");
        int n = scanner.nextInt();
        if (n == 80) {
            System.out.println("It will take longer to compute Fibonacci number ");
        } else if (n >= 100){
            System.out.println("It will take 50 thousand years to calculate this number");
        }
        System.out.println("Fibonacci number "+ n + " = " + fibonachi(n));
    }
    public static long fibonachi( int n){
        if (n <= 1) {
            return n;
        }
        if (n == 2) {
            return 1;
        } else {
            return (fibonachi(n - 2) * 2) + fibonachi(n - 3);
                }
            }
        }