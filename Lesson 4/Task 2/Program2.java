package HomeWork4;

import java.util.Arrays;
import java.util.Scanner;

public class Program2 {
    int binarySearch(int[] arr, int l, int r, int x) {
        if (r >= l) {
            System.out.println("Binary search: from " + l + " to " + r);
            int middle = l + (r - l) / 2;
            if (arr[middle] == x)
                return middle;
            if (arr[middle] > x)
                return binarySearch(arr, l, middle - 1, x);
            return binarySearch(arr, middle + 1, r, x);
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Program2 recursive = new Program2();
        System.out.println("Enter quantity of elements: ");
        int n = scanner.nextInt();
        int[] array = new int[n];
        System.out.println("Enter elements: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println("Entered array: " + Arrays.toString(array));
        System.out.print("Enter number for search: ");
        int number = scanner.nextInt();
        int left = 0;
        int right = array.length - 1;
        int result = recursive.binarySearch(array, left, right, number);
        if (result == -1) {
            System.out.println("Number is not present");
        } else {
            System.out.println("Search is complete. Number found at index [" + result + "]" );
        }
    }
}

