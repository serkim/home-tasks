package HomeWork4;

import java.util.Scanner;

public class Program1 {
    public static int recursion(double number) {
        if (number == 1) {
            return 1;
        } else if (number > 1 && number < 2) {
                return 0;
        } else {
            return recursion(number / 2);
        }
    }
    public static void main(String[] args) {
        System.out.println("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        double number = scanner.nextDouble();
        while (number == 0) {
            System.out.println("You entered 0. Please try again");
            System.out.println("Enter the number: ");
            number = scanner.nextDouble();
        }
        if (recursion(number) == 1) {
            System.out.println("The number is a power of two");
        } else {
            System.out.println("The number is not a power of two");
        }
    }
}