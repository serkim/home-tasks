package HomeWork2;

public class Program3 {
    public static void main(String[] args) {
        int[] array = {4, 2, 3, 5, 7};
        int number = 0;

        for (int i = array.length - 1, n = 0; i >= 0; --i, n++) {
            int a = (int) Math.pow(10, i);
            number += array[n] * a;
        }
        System.out.println(number);
    }
}

