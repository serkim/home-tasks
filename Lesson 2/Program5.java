package HomeWork2;

import java.util.Scanner;

public class Program5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter quantity elements in a line: ");
        int a = scanner.nextInt();
        System.out.println("Enter quantity elements in a column: ");
        int b = scanner.nextInt();
        int[][] array = new int[a][b];
        int step = 1;

        for (int y = 0; y < a; y++) {
            array[0][y] = step;
            step++;

        }
        for (int x = 1; x < b; x++) {
            array[x][a - 1] = step;
            step++;
        }
        for (int y = a - 2; y >= 0; y--) {
            array[b - 1][y] = step;
            step++;
        }
        for (int x = b - 2; x > 0; x--) {
            array[x][0] = step;
            step++;
        }
        int c = 1;
        int d = 1;

        while (step < b * a) {

            while (array[c][d + 1] == 0) {
                array[c][d] = step;
                step++;
                d++;
            }

            while (array[c + 1][d] == 0) {
                array[c][d] = step;
                step++;
                c++;
            }

            while (array[c][d - 1] == 0) {
                array[c][d] = step;
                step++;
                d--;
            }

            while (array[c - 1][d] == 0) {
                array[c][d] = step;
                step++;
                c--;
            }
        }
        for (int x = 0; x < b; x++) {
            for (int y = 0; y < a; y++) {
                if (array[x][y] == 0) {
                    array[x][y] = step;
                }
            }
        }
        System.out.println("Entered array: [" + a +"] ["+ b +"]");
        System.out.println(" ");
        System.out.println("Filled array in a spiral: ");

        for (int x = 0; x < b; x++) {
            for (int y = 0; y < a; y++) {
                if (array[x][y] < 10) {
                    System.out.print(array[x][y] + "  ");
                } else {
                    System.out.print(array[x][y] + " ");
                }
            }
            System.out.println(" ");
        }
    }
}
